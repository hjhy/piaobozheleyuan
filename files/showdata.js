//showdata.js v0.0.7

(function()
{
    XMLHttpData();
})();

function showData( dd )
{
 // alert(dd);
  if(!document.getElementById("musicList")){
        var musicListDom = document.createElement("div");
        musicListDom.id = "musicList";
        musicListDom.className = "";//"music-list hidden";
        document.getElementsByTagName("body")[0].appendChild(musicListDom);
        //是否插入歌曲列表结构
        var html = "";
            html += "<div class=\"list-title\">";
            html += "<strong id=\"listName\"></strong>";
            html += "<span id=\"cutoverList\" class=\"cutover-list hidden\"></span>";
            html += "<div id=\"closeList\" class=\"close-list\" title=\"关闭列表\"></div>";
            html += "</div>";
            html += "<ul id=\"listWrap\" class=\"list-wrap\" data-temp=\"\"></ul>";
        document.getElementById("musicList").innerHTML = "";

	var song = dd, 
        albumNum = song.songData.length,
        html = "";
        //循环数据并插入
        for(var i=0;i<albumNum;i++){
            html += "<li class=\"s-l\">";
            html += "<img src=\"" + song.songData[i].albumCoverMax + "\" class=\"album-cover\">";
            html += "<span class=\"album-name\" title=\"" + song.songData[i].albumName + "\">";
            html += "<span class=\"speaker\"></span><span class=\"s-a\">" + song.songData[i].albumName + "</span></span>";
            html += "<div class=\"album-cover-hover hidden\">";
            html += "<div class=\"mask\" title=\"双击打开\"></div>";
            html += "<span class=\"album-song-num\" title=\"" + song.songData[i].albumSong.length + "首歌曲\">";
            html += song.songData[i].albumSong.length + "</span>";
            html += "<div class=\"play-btn\" title=\"播放专辑：" + song.songData[i].albumName + "\">";
            html += "<div class=\"play-icon\"></div>播放专辑</div>";
            html += "</div>";
            html += "</li>";
        } 
        document.getElementById("musicList").innerHTML = html;

    }
}

function XMLHttpData()
{
    var xmlhttp, dd;

    loadXMLDoc("files/data.js");

    function loadXMLDoc(url){
        xmlhttp = null;
        if(window.XMLHttpRequest){
            xmlhttp = new XMLHttpRequest();
        }
        //IE5,6
        else if(window.ActiveXObject){
            xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
        }
        if(xmlhttp != null){
            xmlhttp.onreadystatechange = getXMLHttpData;
            xmlhttp.open("get", url, true);
            xmlhttp.send(null);
        }else{
            alert("您的浏览器不支持 XMLHTTP");
        }
    }

    function getXMLHttpData(){
        if(xmlhttp.readyState === 4){
            if(xmlhttp.status === 0 || xmlhttp.status === 200){
                dd = eval("(" + xmlhttp.responseText + ")");
                //
                showData(dd);

            }else{
                alert("xddbg: XMLHTTP error，try to reload again!");
            }
        }
    }
}

